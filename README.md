# Album Work Queue

# Setup

Choose a machine to run this on and record its hostname and/or IP
(e.g. `192.168.1.1`). Use this to replace every occurrence of
`YOUR_HOSTNAME` in these instructions.

Setup a `password` file in this directory and on the remote album work
queue directory.

## Setup the cluster

On `dask-scheduler` run:

```
CATALOG_SERVER="YOUR_HOSTNAME" catalog_server
--update-host=YOUR_HOSTNAME --name="album-work-queue"
```

You can then check the status of the work queue manager at:

`http://YOUR_HOSTNAME:9097/`

## Submitting tasks

Define a job file `my_job.py` like:

```
import album_work_queue as awq
import work_queue as wq

q = awq.create_default_work_queue("task name", manager_host=YOUR_HOSTNAME)

def task_fn(args):
  pass
  
for args in arglist:
  t = wq.PythonTask(task_fn, args)
  t.specify_cores(6)
  t.specify_memory(12000000)
  t.specify_gpus(0)
  q.submit(t)
  
kc.wait(q)
```

Then within the `album_work_queue` environment run:

`album-work-queue-submit my_job.py`

To submit a job where each task uses 6 cores, 12gb of memory and no GPU

`ssh YOUR_HOSTNAME "source ~/miniconda3/etc/profile.d/conda.sh && conda activate album_work_queue && cd ~/git/album-app/album-work-queue && git pull && python test_task.py"`

## Launching workers

Within the task's environment:

`work_queue_worker --catalog=YOUR_HOSTNAME:9097 -M test001 --password=password`

Launch a worker on any target machine

`ssh SOME_POTENTIAL_WORKER "source ~/miniconda3/etc/profile.d/conda.sh && conda activate album_work_queue && cd ~/git/album-app/album-work-queue && work_queue_worker --catalog=YOUR_HOSTNAME:9097 -M test001 --password=password"`

TODO:

- align slurm args and wq args for memory and cpu and gpu
- make sure tasks resume when failed
- make a pip package so this can be installed as a pip git (then assume ndcctools are installed via conda)
- `slurm_submit_workers`
- work-queue plugin for emacs
