import album_work_queue as awq

q = awq.create_default_work_queue("test_task")


def task_fn(args):
    x, y = args
    return x + y


arglist = zip(range(100), range(100))

awq.submit_arglist(q, task_fn, arglist)

awq.wait(q)
