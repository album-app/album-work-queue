import os
import datetime

import work_queue as wq


# Queue config (constant for all jobs)
def create_default_work_queue(task_name,
                              password_file='password',
                              manager_host='127.0.0.1',
                              work_queue_port=9097):
    """Create a default WorkQueue for Album work queue"""
    os.environ['CATALOGHOST'] = manager_host

    # q = wq.WorkQueue(port=work_queue_port, ssl_key='MY_KEY.pem', ssl_cert='MY_CERT.pem')
    q = wq.WorkQueue(name=task_name)
    q.specify_catalog_server(manager_host, work_queue_port)
    q.specify_password_file(password_file)

    print("listening on port {}".format(q.port))

    return q


def report_task(task):
    """
    Report the completion status of a task.
    """
    print("task {} completed with result {} took {} at {} on hostname {}, {}".
          format(task.id, task.output,
                 task.total_cmd_execution_time / 1000000.0,
                 datetime.datetime.now().strftime("%A, %d. %B %Y %I:%M:%S%p"),
                 task.hostname, task.host))
    if task.return_status != 0:
        print('command', task.command)
        print('std_output', task.std_output)
        print('result', task.result)
        print('result_status', task.return_status)
        print('result_str', task.result_str)
        print('limits_exceeded', task.limits_exceeded)


def submit_arglist(q,
                   task_fn,
                   arglist,
                   cores=1,
                   memory=1000,
                   debug=False,
                   hungry_wait=True):
    """Submit task_fn to q for each arg in arglist"""

    # Submit several tasks for execution:
    print('submitting ', len(arglist), ' tasks...')
    for args in arglist:
        if debug:
            print('submitting', args)
        task = wq.PythonTask(task_fn, args)
        task.specify_cores(cores)
        task.specify_memory(memory)
        q.submit(task)
        if hungry_wait and not q.hungry():
            task = q.wait(1)
            if task:
                report_task(task)
    print('submitted ', len(arglist), 'tasks')


def wait(q):
    """Wait for the queue q to be done"""

    # As they complete, display the results:
    print("waiting for tasks to complete...")

    while not q.empty():
        task = q.wait(5)
        if task:

            report_task(task)

    print("all done.")
